<?php


namespace App\Tests\Cores;

use PHPUnit\Framework\TestCase;
use App\Controller\FusionController;
use Port\Csv\CsvReader;
use Port\Csv\CsvWriter;


class FusionTest extends TestCase
{
  //Verifier les fichiers sont correct
    public function testfusionSeqencielCsv()
    {

        $csv1 = new \SplFileObject('C:\ProjetSymfony\DeveloppementUnLogiciel\sparkline\public\data\fr-data.csv');
        $csv2 = new \SplFileObject('C:\ProjetSymfony\DeveloppementUnLogiciel\sparkline\public\data\ger-data.csv');
        //$csv3 =  new \SplFileObject('C:\ProjetSymfony\DeveloppementUnLogiciel\sparkline\public\data\fr-ger.csv');



        $reader1 = new CsvReader($csv1, ',');
        $reader1->setHeaderRowNumber(0);
        $reader2 = new CsvReader($csv2, ',');
        $reader2->setHeaderRowNumber(0);


        $writer = new CsvWriter(',', '"');
        $writer->setStream(fopen('C:\ProjetSymfony\DeveloppementUnLogiciel\sparkline\public\data\fr-ger-data.csv', 'w+'));

        //reader1
        $tab =[];
        foreach ($reader1 as $line){
            $tab[]=$line;
        }
        $size = count($tab);

        for($i=1; $i<$size ;$i++)
        {
            $writer->writeItem($tab[$i]);
        }
        //reader 2
        $tab =[];
        foreach ($reader2 as $line){
            $tab[]=$line;
        }

        $size = count($tab);

        for($i=1; $i<$size ;$i++)
        {
        }
        $writer->writeItem($tab[$i]);

        //dd($tab,$reader1,$reader2);
        return $this->render('/fusion/fusion.html.twig');



    }
        public function getLine($file, int $row){
        $tab=[];
        $reader = new CsvReader($file);
        $data = $reader->getRow($row);
        foreach ($data as $value){
            $tab[]= $value;
        }
        return $tab;
    }





}