<?php

namespace App\Controller;


use App\Service\Merge;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\FileUploader;
use App\Form\FileUploadType;
use Psr\Log\LoggerInterface;

class UploadController extends AbstractController
{

    private $DIR;
    /**
     * @Route("/upload", name="upload")
     * @param Request $request
     * @param FileUploader $uploader
     * @return
     */
    public function Upload(Request $request, FileUploader $file_uploader ): Response
    {
        $form = $this->createForm(FileUploadType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            //FileCsv1
            $file1 = $form['upload_file1']->getData();
            if ($file1)
            {
                $file_name1 = $file_uploader->upload($file1);
                if (null !== $file_name1) {
                    $directory = $file_uploader->getTargetDirectory();
                    $fileOriginel1 = $file1->getClientOriginalName();
                    $full_path = $directory . '/' . $file_name1;
                    rename($full_path, $directory . '/' . $fileOriginel1);
                }
                else
                {
                    echo("Oups, an error occured !!!");
                }

                //FileCsv2
                $file2 = $form['upload_file2']->getData();
                if ($file2)
                {
                    $file_name2 = $file_uploader->upload($file2);
                    if (null !== $file_name2) {
                        $directory = $file_uploader->getTargetDirectory();
                        $fileOriginel2 = $file2->getClientOriginalName();
                        $full_path = $directory . '/' . $file_name2;
                        rename($full_path, $directory . '/' . $fileOriginel2);
                    }
                    else
                    {
                        echo("Oups, an error occured !!!");
                    }
                }

                $typeFusion = $form['typeOfFusion']->getData();
                return $this->redirectToRoute('fusion', ['typeOfFusion' => $typeFusion, 'file1' => $fileOriginel1, 'file2' => $fileOriginel2, 'dir'=>$directory]);
            }
       }
        return $this->render('app/upload.html.twig', [
                'form' => $form->createView(),]);

    }

    /**
     * @Route("/fusion", name="fusion")
   */
    public function fusionFile(Request $request)
    {

        $typeFusion = $request->query->get('typeOfFusion');
        $fileName1 = $request->query->get('file1');
        $fileName2 = $request->query->get('file2');
        $directory = $request->query->get('dir');

        $merge = new Merge($directory,$fileName1,$fileName2);

        if($typeFusion === 'intertwinedFusion')
        {
            $merge->intertwined();
        }else{
            $merge->sequential();
        }
        unlink($directory.'/'.$fileName1);
        unlink($directory.'/'.$fileName2);
        $merge->download();
        return $this->redirectToRoute('upload');
    }


}
