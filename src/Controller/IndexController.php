<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\Merge;

class IndexController extends AbstractController
{
    /**
     * @Route("/home", name="Index")
     */
    public function index()
    {

        return $this->redirectToRoute('upload');
    }


    /**
     * @Route ("/downloadTheMergeFile", name="downloadMergeFile")
     */
    public function dowloandTheMergeFile()
    {
        $filePath = "";
        $files =glob($this->targerDirectory.'mergedFile\*');
        foreach ($files as $file)
        {
             if(is_file($file))
             {
                 $filePath = $file;
             }
        }
        $respose = new BinaryFileResponse($filePath);
        $respose->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        $respose->headers->set('Content-type','txt/csv');


        return $respose;
    }




    /**
     * @Route ("/etl", name="etl")
     */
    public function ELT()
    {
        $filetoImport = $this->findMergedfind();
        return $this->render(''); //crée un page twig
    }


}