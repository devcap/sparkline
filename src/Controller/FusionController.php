<?php


namespace App\Controller;

use App\Service\Merge;
use Port\Csv\CsvReader;
use Port\Csv\CsvWriter;

class FusionController
{
    public function sequential()
    {
        $pathcsv1 = new \SplFileObject($this->targetDirectory. '/' .$this->fileName1);
        $pathcsv2 = new \SplFileObject($this->targetDirectory. '/' .$this->fileName2);

        $csvFile1 = new CsvReader($pathcsv1, ',');
        $csvFile1->setHeaderRowNumber(0);

        $csvFile2 = new CsvReader($pathcsv2, ',');
        $csvFile2->setHeaderRowNumber(0);
        $this->fileResult = 'fr-ger-data'.date('YmdHis').'.csv';
        $writer = new CsvWriter(',', '"');
        $writer->setStream(fopen($this->targetDirectory.'/'.$this->fileResult, 'w+'));
        $writer->writeItem(self::COLUMN_HEADER);


        $tab1 =[];
        foreach ($csvFile1 as $line)
        {
            $tab1[]=$line;
        }
        $size = count($tab1);
        $j = 0;
        for($i=0; $i<$size ;$i++)
        {
            $writer->writeItem([(++$j),$tab1[$i]['Gender'], $tab1[$i]['Title'], $tab1[$i]['GivenName'], $tab1[$i]['Surname'],
                $tab1[$i]['StreetAddress'], $tab1[$i]['City'],$tab1[$i]['ZipCode'], $tab1[$i]['Country'], $tab1[$i]['EmailAddress'], $tab1[$i]['Birthday'], $tab1[$i]['CCType'], $tab1[$i]['CCNumber'],
                $tab1[$i]['CCExpires'], $tab1[$i]['Vehicle'],$tab1[$i]['Pounds'],$tab1[$i]['Latitude'],$tab1[$i]['Latitude']]);
        }


        //reader 2
        $tab2 =[];
        foreach ($csvFile2 as $line)
        {
            $tab2[]=$line;
        }
        $size = count($tab2);

        for($i=0; $i<$size ;$i++)
        {
            $writer->writeItem([(++$j),$tab2[$i]['Gender'], $tab2[$i]['Title'], $tab2[$i]['GivenName'], $tab2[$i]['Surname'],
                $tab2[$i]['StreetAddress'], $tab2[$i]['City'],$tab2[$i]['ZipCode'], $tab2[$i]['Country'], $tab2[$i]['EmailAddress'], $tab2[$i]['Birthday'], $tab2[$i]['CCType'], $tab2[$i]['CCNumber'],
                $tab2[$i]['CCExpires'], $tab2[$i]['Vehicle'],$tab2[$i]['Pounds'],$tab2[$i]['Latitude'],$tab2[$i]['Latitude']]);

        }
    }
    public function intertwined()
    {
        $pathcsv1 = new \SplFileObject($this->targetDirectory. '/' .$this->fileName1);
        $pathcsv2 = new \SplFileObject($this->targetDirectory. '/' .$this->fileName2);

        $csvFile1 = new CsvReader($pathcsv1, ',');
        $csvFile1->setHeaderRowNumber(0);

        $csvFile2 = new CsvReader($pathcsv2, ',');
        $csvFile2->setHeaderRowNumber(0);


        $writer = new CsvWriter(',', '"');
        $this->fileResult = 'fr-ger-entrelace'.date('YmdHis').'.csv';
        $writer->setStream(fopen($this->targetDirectory.'/'.$this->fileResult, 'w+'));
        $writer->writeItem(self::COLUMN_HEADER);

        //remplissage du premier fichier csv
        $tab1 = [];
        foreach ($csvFile1 as $line){
            $tab1[]=$line;
        }
        $size1 = count($tab1);

        //remplissage du deuxieme fichier csv
        $tab2 =[];
        foreach ($csvFile2 as $line){
            $tab2[]=$line;
        }
        $size2 = count($tab2);

        if($size1 > $size2){
            $sizeFin= $size1;
        }else{
            $sizeFin = $size2;
        }
        $j = 0;
        for($i=1; $i<$sizeFin + 1 ;$i++)
        {
            if ($i < $size1)
            {
                $writer->writeItem([(++$j),$tab1[$i]['Gender'], $tab1[$i]['Title'], $tab1[$i]['GivenName'], $tab1[$i]['Surname'],
                    $tab1[$i]['StreetAddress'], $tab1[$i]['City'],$tab1[$i]['ZipCode'], $tab1[$i]['Country'], $tab1[$i]['EmailAddress'], $tab1[$i]['Birthday'], $tab1[$i]['CCType'], $tab1[$i]['CCNumber'],
                    $tab1[$i]['CCExpires'], $tab1[$i]['Vehicle'],$tab1[$i]['Pounds'],$tab1[$i]['Latitude'],$tab1[$i]['Latitude']]);
            }
            if($i < $size2)
            {
                $writer->writeItem([(++$j),$tab2[$i]['Gender'], $tab2[$i]['Title'], $tab2[$i]['GivenName'], $tab2[$i]['Surname'],
                    $tab2[$i]['StreetAddress'], $tab2[$i]['City'],$tab2[$i]['ZipCode'], $tab2[$i]['Country'], $tab2[$i]['EmailAddress'], $tab2[$i]['Birthday'], $tab2[$i]['CCType'], $tab2[$i]['CCNumber'],
                    $tab2[$i]['CCExpires'], $tab2[$i]['Vehicle'],$tab2[$i]['Pounds'],$tab2[$i]['Latitude'],$tab2[$i]['Latitude']]);
            }
        }
    }
}