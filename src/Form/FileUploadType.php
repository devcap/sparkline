<?php

namespace App\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class FileUploadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('upload_file1', FileType::class, [
                'label' => false,
                'mapped' => false, // Tell that there is no Entity to link
                'required' => true,
                'constraints' => [
                    new File([
                        'mimeTypes' => [ // We want to let upload only txt, csv or Excel files
                            'text/x-csv',
                            'text/csv',
                            'text/plain',
                            'application/x-csv',
                            'application/csv',
                        ],
                        'mimeTypesMessage' => "This document isn't valid.",
                    ])
                ],
            ])
            ->add('upload_file2', FileType::class, [
                'label' => false,
                'mapped' => false, // Tell that there is no Entity to link
                'required' => true,
                'constraints' => [
                    new File([
                        'mimeTypes' => [
                            'text/x-csv',
                            'text/csv',
                            'text/plain',
                            'application/x-csv',
                            'application/csv',
                        ],
                        'mimeTypesMessage' => "This document isn't valid.",
                    ])
                ],
            ])

            ->add('typeOfFusion', choiceType::Class,[
                   'choices'=>[
                           'Sequentiel'=> 'sequentielFusion',
                           'intertwined'=> 'intertwinedFusion'
                   ]
          ])

            ->add('send', SubmitType::class);
    }

}
